const express = require('express')
const app = express()
const axios = require('axios').default
const PORT = 5000
const CLIENT_ID = 'bevhu9u7cup97dpvf1eg'
const CLIENT_SECRET = '6265766875397537637570393764707666316630'
const REDIRECT_URI = 'http://localhost:5000/cb'
const SCOPE = 'read+write'
const RESPONSE_TYPE = 'code'
let currentToken = null


app.set('view engine','pug')


app.get('/example/resourceOwner',(req,res)=>{
    axios.post(`http://localhost:8989/oauth/token`,`grant_type=password&username=aa1&password=aa1`,{
        headers:{
            'content-type':'application/x-www-form-urlencoded',
        },
        auth:{
            username:CLIENT_ID,
            password:CLIENT_SECRET
        }
    })
        .then(r=>{
            res.json(r.data)
        })
        .catch(err=>{
            console.error(err.response)
            res.status(err.response.status).json(err.response.data)
        })
})

app.get('/cb',(req,res)=>{ 
    const {code} = req.query
    axios.post(`http://localhost:8989/oauth/token`,
    `grant_type=authorization_code&code=${code}&client_id=${CLIENT_ID}&client_secret=${CLIENT_SECRET}&redirect_uri=${REDIRECT_URI}`,{
        headers:{
            'content-type':'application/x-www-form-urlencoded'
        }
    })
        .then(r=>{
            const {data} = r
            currentToken = data
            return data
        })
        .then(data=>{ 
            // after get token, use it to hit resource server API
            return axios.get(`http://localhost:6000/user/${data.username}`,{
                headers:{
                    'Authorization':`Bearer ${data.token}`
                },
            })
        })
        .then(x=>{
            console.log(x.data)
            res.json(x.data)
        })
        .catch(err=>{
            console.log(err.message)
            res.status(500).json(...err.message)
        })
})


app.get('/',(req,res)=>{
    res.render('login')
})

app.get('/login',(req,res)=>{
    res.redirect(`http://localhost:8989/oauth/auth?client_id=${CLIENT_ID}&redirect_uri=${REDIRECT_URI}&scope=${SCOPE}&response_type=${RESPONSE_TYPE}`)
})



app.listen(PORT, ()=> console.log(`client's server listening on port ${PORT}`))