const express = require('express')
const app = express()
const PORT = 6000
const Sequelize = require("sequelize")
const axios = require('axios').default

const sequelize = new Sequelize('golang_db','arief','arief',{
    operatorsAliases:false,
    dialect:'mysql'
})
const UserModel = sequelize.define('user',{
    username:{
        type:Sequelize.STRING,
        primaryKey:true,
    },
    password:{
        type:Sequelize.STRING,
        allowNull:false,
    }
},{
    tableName:'user',
    timestamps:false
})

sequelize.sync()


app.get('/user',(req,res)=>{
    const authHeader = req.header('Authorization')
    const token = authHeader.split(' ')[1]
    
    axios.post(`http://localhost:8989/oauth/introspect`,`token=${token}`)
    .then(r=>{
        return r.data
    })
    .then(data=>{
        const {active} = data
        if(!active) {
            res.status(403).json({
                code:403,
                data:'INVALID_TOKEN'
            })
            return
        }
        return UserModel.findAll()
    })
    .then(m=>{
        res.json({
            code:200,
            data:m
        })
    })
    .catch(err=>{
        console.error(`err ${req.path}`,err.message)
        res.status(500).json({
            code:500,
            data:'Server cannot proceed this request'
        })
    })
    
})

app.get('/user/:username',(req,res)=>{ 
    const authHeader = req.header('Authorization')
    const token = authHeader.split(' ')[1]

    // validate token (hit /introspection API in Authorization Server)
    axios.post(`http://localhost:8989/oauth/introspect`,`token=${token}`)
        .then(r=>{
            return r.data
        })
        .then(data=>{
            const {active} = data
            if(!active) {
                res.status(403).json({
                    code:403,
                    data:'INVALID_TOKEN'
                })
                return
            }
            return UserModel.findById(req.params.username)
        })
        .then(m=>{
            res.json({
                code:200,
                data:m
            })
        })
        .catch(err=>{
            console.error(`err ${req.path}`,err.message)
            res.status(500).json({
                code:500,
                data:'Server cannot proceed this request'
            })
        })
})

app.listen(PORT , ()=> console.log(`resource server running on ${PORT}`))


