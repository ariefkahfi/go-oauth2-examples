package model


type Client struct {
	ClientId string `json:"client_id"`
	ClientSecret string `json:"client_secret"`

	RedirectURI []string `json:"redirect_uris"`
	TokenEndpointAuthMethod string `json:"token_endpoint_auth_method"`
	GrantType []string `json:"grant_types"`
	ResponseType []string `json:"response_types"`
	ClientName string `json:"client_name"`
	Scope string `json:"scope"`
	ClientSecretExpiresAt int `json:"client_secret_expires_at"`
}

func NewClient(
	clientId,
	clientSecret,
	tokEndAuMeth,
	cliName,
	scope string,
	resType,
	graType ,
	redirectUri []string,
	clientSecretExpiresAt int,
) Client {
	return Client{
		ClientId:clientId,
		ClientSecret:clientSecret,
		TokenEndpointAuthMethod:tokEndAuMeth,
		GrantType:graType,
		ResponseType:resType,
		ClientName:cliName,
		Scope:scope,
		RedirectURI:redirectUri,
		ClientSecretExpiresAt: clientSecretExpiresAt,
	}
}