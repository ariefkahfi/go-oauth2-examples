package model

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"github.com/go-redis/redis"
	"gopkg.in/boj/redistore.v1"
)


var (
	DB , ERR_DB = sql.Open("mysql","arief:arief@/golang_db")
	RedisClient = redis.NewClient(&redis.Options{
		Addr:":6379",
		DB:0,
	})
	RediStore, ERR_REDIS_STORE = redistore.NewRediStore(10 , "tcp",":6379","",[]byte("KEY123"))
)