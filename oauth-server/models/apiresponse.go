package model


type ApiResponse struct {
	Code int `json:"code"`
	Data interface{} `json:"data"`
}

func NewApiResponse(code int , data interface{}) ApiResponse {
	return ApiResponse{ 
		code,data,
	}
}