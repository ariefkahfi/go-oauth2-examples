package model

type ClientError struct {
	ErrorCode string `json:"error"`
	ErrorDescription string `json:"error_description"`
}

func NewClientErrorTemporarilyUnavailable(errDesc string) ClientError {
	return ClientError {
		"temporarily_unavailable",
		errDesc,
	}
}

func NewClientError(errCode , errDesc string) ClientError {
	return ClientError{
		errCode,
		errDesc,
	}
}

