package main


import (
	"net/url"
	"database/sql"
	"fmt"
	"strings"
	"encoding/json"
	"io/ioutil"
	"time"
	"net/http"
	"github.com/gorilla/mux"
	"encoding/hex"
	"github.com/rs/xid"
	"oauth2-projects/auth-server/oauth-server/models"
	"oauth2-projects/auth-server/oauth-server/flows"
)

var (
	db = model.DB
	rediStore = model.RediStore
)


func RegisterHandler(w http.ResponseWriter, r *http.Request){
	cleId := xid.New().String()
	rawCleSecret := xid.NewWithTime(time.Now()).String()
	rawToHex := hex.EncodeToString([]byte(rawCleSecret))

	rdAll, _ := ioutil.ReadAll(r.Body)
	
	var nClient model.Client
	json.Unmarshal(rdAll , &nClient)


	w.Header().Set("Content-type","application/json")

	nClient.ClientId = cleId
	nClient.ClientSecret = rawToHex
	nClient.ClientSecretExpiresAt = 0


	clientRedUris := nClient.RedirectURI
	clientGraTypes := nClient.GrantType
	clientResTypes := nClient.ResponseType

	jCLientRedUris := strings.Join(clientRedUris , ";")
	jClientGraTypes := strings.Join(clientGraTypes , ";")
	jClientResTypes := strings.Join(clientResTypes , ";")


	_ , err := db.Exec("insert into oauth2_clients values (?,?,?,?,?,?,?,?,?)",
		nClient.ClientId,
		nClient.ClientSecret,
		jCLientRedUris,
		nClient.TokenEndpointAuthMethod,
		jClientGraTypes,
		jClientResTypes,
		nClient.ClientName,
		nClient.Scope,
		nClient.ClientSecretExpiresAt,
	)
	if err != nil {
		apiResponse := model.NewApiResponse(500,"ERR_INTERNAL_SERVER")
		w.WriteHeader(http.StatusInternalServerError)
		b , _ := json.Marshal(apiResponse)
		w.Write(b)
		return
	}


	
	b , _ := json.Marshal(nClient)
	w.Write(b)
}


func main(){
	if model.ERR_DB != nil {
		fmt.Println("Application Stopped {Database error} !!!")
		panic(model.ERR_DB)
	}

	if model.ERR_REDIS_STORE != nil {
		fmt.Println("Application Stopped {Redis error} !!! ")
		panic(model.ERR_REDIS_STORE)
	}


	r := mux.NewRouter()



	r.Path("/oauth/introspect").Methods("POST").
		HandlerFunc(handlers.IntrospectHandler)
	r.Path("/oauth/token").Methods("POST").
		HandlerFunc(handlers.TokenHandler)
	r.Path("/oauth/auth").Methods("GET").
		HandlerFunc(handlers.AuthorizationCodeHandler)
	r.Path("/oauth/auth").Methods("POST").
		HandlerFunc(func(w http.ResponseWriter, r *http.Request){
			username := r.FormValue("username")
			password := r.FormValue("password")
			rawQuery := r.URL.RawQuery
			v , _ := url.ParseQuery(rawQuery)

			qRedirectUri := v.Get("redirect_uri")
			qClientId := v.Get("client_id")
			qScope := v.Get("scope")
			qResponseType := v.Get("response_type")

			row := db.QueryRow("select * from user where username = ? and password = ? ",username,password)
			err := row.Scan(&username, &password)

			if err == sql.ErrNoRows {
				http.Redirect(w ,r , "/oauth/auth?client_id="+qClientId+"&redirect_uri="+qRedirectUri+"&scope="+qScope+"&response_type="+qResponseType,302)
				return
			}

			s , _ := rediStore.Get(r , "oauth-store")
			s.Values["username"] = username
			err = s.Save(r , w)
			if err != nil {
				fmt.Println("session_save error")
			}

			http.Redirect(w ,r , "/oauth/auth?client_id="+qClientId+"&redirect_uri="+qRedirectUri+"&scope="+qScope+"&response_type="+qResponseType,302)
		})
	r.Path("/oauth/register").Headers("Content-type","application/json").Methods("POST").
		HandlerFunc(RegisterHandler)
	
	http.ListenAndServe(":8989",r)
}