package handlers

import (
	"time"
	"github.com/rs/xid"
	"encoding/json"
	"oauth2-projects/auth-server/oauth-server/models"
	"database/sql"
	"net/http"
)

func clientCredentialHandler(w http.ResponseWriter, r *http.Request){
	usr , pass , ok := r.BasicAuth()
	w.Header().Set("content-type","application/json")

	if !ok {
		w.WriteHeader(400)
		errResp := model.NewClientError("invalid_request","Bad request encountered")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	row := db.QueryRow("select * from oauth2_clients where client_id = ? and client_secret = ?",usr , pass)
	err := row.Scan(&usr , &pass)
	if err == sql.ErrNoRows {
		w.WriteHeader(401)
		errResp := model.NewClientError("invalid_client","Unknown client identifier or secret")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	newToken := "A_"+xid.New().String()
	mapToken := map[string]interface{}{
		"access_token":newToken,
		"expires":3600,
	}
	b , _ := json.Marshal(mapToken)
	redisClient.Set("token:"+newToken , string(b) , time.Hour)
	w.Write(b)
}