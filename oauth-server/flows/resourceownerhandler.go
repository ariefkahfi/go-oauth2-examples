package handlers

import (
	"fmt"
	"time"
	"github.com/rs/xid"
	"net/http"
	"oauth2-projects/auth-server/oauth-server/models"
	"encoding/json"
	"database/sql"
)


func resourceOwnerHandler(w http.ResponseWriter, r *http.Request) {
	usr , pass , ok := r.BasicAuth()
	fmt.Println(usr , pass)

	oUsername := r.FormValue("username")
	oPassword := r.FormValue("password")

	if !ok {
		w.WriteHeader(400)
		errResp := model.NewClientError("invalid_request","Bad request encountered")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	row := db.QueryRow("select * from oauth2_clients where client_id = ? and client_secret = ?",usr , pass)
	err := row.Scan(&usr , &pass)
	if err == sql.ErrNoRows {
		w.WriteHeader(401)
		errResp := model.NewClientError("invalid_client","Unknown client identifier or secret")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	row = db.QueryRow("select * from user where username = ?  and password = ? ",oUsername , oPassword)
	err = row.Scan(&oUsername , &oPassword)
	if err == sql.ErrNoRows {
		w.WriteHeader(500)
		errResp := model.NewClientError("server_error","Invalid username or password")
		b , _ := json.Marshal(errResp)
		w.Write(b)
		return
	}

	newToken := "A_"+xid.New().String()
	refreshToken := "R_"+xid.New().String()

	mapToken := map[string]interface{}{
		"access_token":newToken,
		"expires":3600,
		"refresh_token":refreshToken,
	}
	b , _ := json.Marshal(mapToken)
	redisClient.Set("token:"+newToken,string(b),time.Hour)
	redisClient.Set("refresh:"+refreshToken , refreshToken , (24*time.Hour))

	w.Write(b)
}