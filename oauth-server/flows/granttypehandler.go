package handlers

import (
	"fmt"
	"time"
	"github.com/rs/xid"
	"encoding/json"
	"oauth2-projects/auth-server/oauth-server/models"
	"net/http"
)

func IntrospectHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("content-type","application/json")
	accessToken := r.FormValue("token")
	mapToken := map[string]interface{}{
		"code":200,
		"active":false,
	}
	_  , err := redisClient.Get("token:"+accessToken).Result()
	if err == nil {
		mapToken["active"] = true

		b , _ := json.Marshal(mapToken)
		w.Write(b)
		return
	}
	b , _ := json.Marshal(mapToken)
	w.Write(b)
}


func TokenHandler (w http.ResponseWriter, r *http.Request) {
	grantType := r.FormValue("grant_type")
	w.Header().Set("content-type","application/json")

	if grantType == "password" {
		resourceOwnerHandler(w,r)
	} else if grantType == "authorization_code" {
		fmt.Println("req_header",r.Header)
		qCode := r.FormValue("code")

		s , err := redisClient.Get("code:"+qCode).Result()
		if err == nil {

			var mapCode map[string]interface{}
			err = json.Unmarshal([]byte(s) , &mapCode)
			if err != nil {
				apiErr := model.NewClientError("server_error","Server cannot proceed this request")
				b , _ := json.Marshal(apiErr)
				w.Write(b)
				return
			}
			username := mapCode["username"].(string)
			code := mapCode["code"].(string)

			redisClient.Del("code:"+code)

	
			newToken := "A_"+xid.New().String()
			refreshToken := "R_"+xid.New().String()
			tokenToDb := map[string]interface{}{
				"access_token":newToken,
				"username": username,
				"refresh_token":refreshToken,
				"expires":3600,
			}
			b , _ := json.Marshal(tokenToDb)
	
			_ , err:= redisClient.Set("token:"+newToken, string(b) , time.Hour).Result()
			if err != nil {
				panic(err)
			}
			_ , err = redisClient.Set("refresh_token:"+refreshToken, refreshToken , (24 * time.Hour)).Result()
			if err != nil {
				panic(err)
			}
	
			w.Write(b)
		} else {
			w.WriteHeader(500)
			apiErr := model.NewClientError("server_error","Server cannot proceed this code")
			b , _ := json.Marshal(apiErr)
			w.Write(b)	
		}


	} else if grantType == "client_credentials" {
		clientCredentialHandler(w , r )
	} else if grantType == "refresh_token" {

	}
}