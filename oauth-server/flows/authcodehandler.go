package handlers

import (
	"time"
	"github.com/rs/xid"
	"oauth2-projects/auth-server/oauth-server/utils"
	"strings"
	"encoding/json"
	"database/sql"
	"net/http"
	"html/template"
	"oauth2-projects/auth-server/oauth-server/models"
	"net/url"
)

var (
	db = model.DB
	redisClient = model.RedisClient
	rediStore = model.RediStore
)

func AuthorizationCodeHandler(w http.ResponseWriter, r *http.Request) {
	rRawQ := r.URL.RawQuery
	v , _ := url.ParseQuery(rRawQ)

	qClientId := v.Get("client_id")
	qRedirectUri := v.Get("redirect_uri")
	qScope := v.Get("scope")
	qResponseType := v.Get("response_type")


	// authenticate client credential
	row := db.QueryRow("select * from oauth2_clients where client_id = ?",qClientId)
	
	var clientId,clientSecret,redirectUris,tokenEndAuthMethod,grantType,responseType,clientName,scope string
	var clientSecretExpiresAt int


	err := row.Scan(
		&clientId,
		&clientSecret,
		&redirectUris,
		&tokenEndAuthMethod,
		&grantType,
		&responseType,
		&clientName,
		&scope,
		&clientSecretExpiresAt,
	)

	if err == sql.ErrNoRows {
		w.Header().Set("content-type","application/json")
		w.WriteHeader(http.StatusInternalServerError)
		apiClientError := model.NewClientError("server_error" , "This client not registered yet !")
		b , _ := json.Marshal(apiClientError)
		w.Write(b)
		return
	}
	arrScope := strings.Split(scope , " ")
	arrRedUris := strings.Split(redirectUris , ";")
	isExists := utils.IsExists(arrRedUris , qRedirectUri)	

	if !isExists {
		w.Header().Set("content-type","application/json")
		w.WriteHeader(http.StatusBadRequest)
		apiClientError := model.NewClientError("invalid_request","Invalid redirect URIs for client")
		b , _ := json.Marshal(apiClientError)
		w.Write(b)
		return
	}
	// authenticate client credential


	s , _ := rediStore.Get(r , "oauth-store")
	username , ok := s.Values["username"]

	if !ok {
		t := template.Must(template.ParseFiles("templates/auth-request.html"))
		t.Execute(w , map[string]interface{}{
			"isLogin":false,
			"client_id":qClientId,
			"redirect_uri":qRedirectUri,
			"scope":qScope,
			"response_type":qResponseType,
		})
		return
	}

	newCode := xid.New().String()
	// save code into redis with (5 minutes TTL)
	mapCode := map[string]interface{}{ 
		"code":newCode,
		"username":username,
	}
	b , _ := json.Marshal(mapCode)
	_ , err = redisClient.Set("code:"+newCode,string(b),(5 * time.Minute)).Result()
	if err != nil {
		w.Header().Set("content-type","application/json")
		w.WriteHeader(http.StatusInternalServerError)
		apiClientError := model.NewClientError("server_error" , "Error generating code")
		b , _ := json.Marshal(apiClientError)
		w.Write(b)
		return
	}

	allowedUri := qRedirectUri+"?code="+newCode
	deniedUri := qRedirectUri+"?error=access_denied"+"&error_description=The+user+denied+the+request"
	

	t := template.Must(template.ParseFiles("templates/auth-request.html"))
	t.Execute(w , map[string]interface{}{
		"client_name":clientName,
		"scope":arrScope,
		"denied_uri":deniedUri,
		"allowed_uri":allowedUri,
		"username":username,
		"isLogin":ok,
	})
}	