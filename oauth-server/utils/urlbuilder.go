package utils





func BuildAuthorizationRequestUrl(
	baseUrl,
	responseType,
	clientId,
	scope,
	redirectUri string,
) string {
	return baseUrl+"?response_type="+responseType+"&client_id="+clientId+"&redirect_uri="+redirectUri+"&scope="+scope	
}